sed -i 's/{{TAG}}/'$TAG'/g' deployment/app.yaml
kubectl create namespace $NAMESPACE
kubectl apply -f deployment/database-credentials.yaml --namespace=$NAMESPACE
kubectl apply -f deployment/database.yaml --namespace=$NAMESPACE
kubectl apply -f deployment/ConfigMap.yaml --namespace=$NAMESPACE
kubectl apply -f deployment/app.yaml --namespace=$NAMESPACE