FROM openjdk:8-alpine
COPY artifacts/assignment-*.jar /app/assignment.jar
ENV spring.profiles.active=mysql
ENV server.port=8090
EXPOSE 8090
CMD java -Xmx1700m -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=7000 -jar /app/assignment.jar
